import { hashObject } from './hash'
import { Block } from './types'

export class Database {
  private block_list: Block[] = []

  async addBlock(block: Block) {
    console.log('add block:', block)
    this.block_list.push(block)
  }

  async getPrevHash(): Promise<string> {
    if (this.block_list.length === 0) {
      return '0'.repeat(64)
    }
    let block = this.block_list[this.block_list.length - 1]
    return hashObject(block)
  }

  async getDifficulty() {
    return '0' + '1'.repeat(63)
  }
}
