import { createHash } from 'crypto'

export function hashObject(object: object): string {
  let json = JSON.stringify(object)
  let hash = createHash('sha256')
  hash.write(json)
  return hash.digest().toString('hex')
}
