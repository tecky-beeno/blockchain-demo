import { Database } from './database'
import { Node } from './node'
import { Store } from './store'

let database = new Database()
let store = new Store(database)
let node1 = new Node('alice', database, store)
let node2 = new Node('bob', database, store)
node1.start()
node2.start()
