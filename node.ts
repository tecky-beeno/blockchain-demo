import { Database } from './database'
import { hashObject } from './hash'
import { Store } from './store'
import { sleep } from './timer'
import { Address, Block, Txn } from './types'

const SleepInterval = 10

export class Node {
  running = false

  constructor(
    private address: Address,
    private database: Database,
    private store: Store,
  ) {}

  async start() {
    this.running = true
    for (; this.running; ) {
      try {
        let block = await this.mine()
        await this.handleBlock(block)
      } catch (error) {
        console.log(error)
      }
    }
  }

  async handleBlock(block: Block) {
    await this.store.handleBlock(block)
    await this.database.addBlock(block)
    this.store.report()
  }

  async mine(): Promise<Block> {
    console.log('start mine')
    let coinTxn: Txn = {
      sender: this.address,
      payload: {
        type: 'coin',
        amount: 1,
      },
    }
    let prev_hash = await this.database.getPrevHash()
    let difficulty = await this.database.getDifficulty()
    for (let nonce = 0; ; nonce++) {
      if (!this.running) {
        throw new Error('mine canceled')
      }
      if (nonce % 100 === 0) {
        console.log('mining, nonce:', nonce)
      }
      let block: Block = {
        prev_hash,
        txn_list: [coinTxn],
        nonce,
      }
      let blockHash = hashObject(block)
      if (blockHash < difficulty) {
        console.log('mined block:', block)
        return block
      }
      await sleep(SleepInterval)
    }
  }

  stop() {
    this.running = false
  }
}
