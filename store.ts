import { Database } from './database'
import { hashObject } from './hash'
import { Address, Block, Txn } from './types'

export class Store {
  prev_hash = '0'.repeat(64)

  account_balance: Record<Address, number> = {}

  constructor(private database: Database) {}

  async handleBlock(block: Block) {
    if (block.prev_hash !== this.prev_hash) {
      throw new Error('invalid prev_hash')
    }

    let blockHash = hashObject(block)
    let difficulty = await this.database.getDifficulty()
    if (blockHash >= difficulty) {
      throw new Error('difficulty failed')
    }

    for (let i = 0; i < block.txn_list.length; i++) {
      await this.handleTxn(block.txn_list[i], i)
    }

    this.prev_hash = blockHash
  }

  private async handleTxn(txn: Txn, index: number) {
    if (txn.payload.type === 'coin') {
      if (index !== 0) {
        throw new Error('invalid coin txn')
      }
      let balance = this.account_balance[txn.sender] || 0
      this.account_balance[txn.sender] = balance + txn.payload.amount
    }
  }

  report() {
    console.log(this.account_balance)
  }
}
