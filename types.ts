export type Address = string
export type Hash = string

export type Txn = {
  sender: Address
  payload: TxnPayload
}

export type TxnPayload =
  | {
      type: 'coin'
      amount: number
    }
  | {
      type: 'create_pool'
      answer_hash: Hash
    }
  | {
      type: 'bid'
      answer: string
    }

export type Block = {
  prev_hash: Hash
  txn_list: Txn[]
  nonce: number
}
